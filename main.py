import time
import machine
import onewire
import ds18x20
from umqtt.simple import MQTTClient

ds = None
roms = None
mqtt_client = None
sta_if = None
ssid = "testing-v2"
password = "BushDid*911"
mqtt_server = 'vm.523034.xyz'
mqtt_port = 1883
client_id = 'esp8266'
topic = 'topic/temp_reading'


def setup():
    global ds, roms

    dat = machine.Pin(5)
    ds = ds18x20.DS18X20(onewire.OneWire(dat))
    roms = ds.scan()
    print('found devices:', roms)


def event_loop():
    while True:
        if not sta_if.isconnected():
            connect_to_wifi()
        print('temperatures:', end=' ')
        ds.convert_temp()
        time.sleep_ms(1000)
        for rom in roms:
            print(ds.read_temp(rom), end=' ')
            mqtt_client.publish(topic, str(ds.read_temp(rom)))
        print()


def connect_to_wifi():
    import network
    global sta_if
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(ssid, password)
        while not sta_if.isconnected():
            pass
        print("wifi connected")


def connect_to_mqtt():
    global mqtt_client
    mqtt_client = MQTTClient(client_id, mqtt_server, port=mqtt_port)
    mqtt_client.connect()


def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()


def main():
    connect_to_wifi()
    try:
        connect_to_mqtt()
    except OSError:
        restart_and_reconnect()
    setup()
    event_loop()


if __name__ == '__main__':
    main()
